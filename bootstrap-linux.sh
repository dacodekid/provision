#!/bin/bash

house_keeping=( 
	update 
	upgrade 
	autoclean )
for i in "${house_keeping[@]}"; do
    echo "${i}"
    eval "sudo apt-get --yes --force-yes ${i}" > /dev/null
done

apt_get=( 
	w3m 
	git git-core 
	python3 python3-pip 
	supervisor nginx 
	libpq-dev python3-dev 
	postgresql postgresql-contrib 
	zsh )
for i in "${apt_get[@]}"; do
    echo "installing ${i}"
    eval "sudo apt-get --yes --force-yes install ${i}" > /dev/null
done

pip3=( virtualenv virtualenvwrapper autoenv )
for i in "${pip3[@]}"; do
    echo "installing ${i}"
    eval "pip3 install ${i}" > /dev/null
done

wget --no-check-certificate http://install.ohmyz.sh -O - | sh
chsh -s $(which zsh)

WORKON_HOME=".envs"
PROJECT_HOME="webapps"
file="$HOME/.zshrc"
sed -i.bak 's/robbyrussell/bira/g' ${file}
if ! grep -qs "WORKON_HOME" "${file}"; then
    {
        echo
        echo "alias zshconfig='nano ${file}'"
        echo "alias ohmyzsh='nano ${file}/.oh-my-zsh'"
        echo "autoload -U promptinit"
        echo "promptinit"

        echo "export WORKON_HOME=/${WORKON_HOME}"
        echo "export PROJECT_HOME=/${PROJECT_HOME}"
        echo "export VIRTUALENVWRAPPER_PYTHON=$(which python3)"
        echo "source /usr/local/bin/virtualenvwrapper.sh"
        #preactivate wont source automaticaly
        echo "source /$WORKON_HOME/preactivate"
        # autoenv        
        echo "source /usr/local/bin/activate.sh"
        echo
    } >> ${file}
fi
source ${file}

mkdir /root/.ssh
cp /vagrant/vagrant_rsa.pub /root/.ssh/authorized_keys