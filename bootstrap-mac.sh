#!/bin/bash

#install xcode
xcode-select --install

# install brew
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
brew doctor

brew_package=(
	zsh
	npm
	nginx
	postgres
	caskroom/cask/brew-cask )
for i in "${brew_package[@]}"; do
    eval "brew install ${i}"
done

brew_tap=(
	caskroom/versions
	caskroom/fonts )
for i in "${brew_tap[@]}"; do
    eval "brew tap ${i}"
done

brew_cask=(
	atom
	google-chrome firefox
	spectacle appcleaner the-unarchiver deluge
	vagrant
	python3
	postgres pgadmin3
	filezilla iterm2 sourcetree macdown dash)
for i in "${brew_cask[@]}"; do
    eval "brew cask install ${i}"
done

pip3=( virtualenv virtualenvwrapper autoenv )
for i in "${pip3[@]}"; do
    eval "pip3 install ${i}"
done

#install oh-my-zsh
curl -L http://install.ohmyz.sh | sh
command -v zsh | sudo tee -a /etc/shells
chsh -s "/usr/local/bin/zsh"

# nginx setup
mkdir -p /usr/local/etc/nginx/sites-{enabled,available}
ln -sfv /usr/local/opt/nginx/*.plist ~/Library/LaunchAgents
launchctl load ~/Library/LaunchAgents/homebrew.mxcl.nginx.plist

# zsh setup
WORKON_HOME=".envs"
PROJECT_HOME="dev"
file="$HOME/.zshrc"
sed -i.bak 's/robbyrussell/bira/g' ${file}
if ! grep -qs "WORKON_HOME" "${file}"; then
	{
		echo "PATH='/Library/Frameworks/Python.framework/Versions/3.4/bin:${PATH}'"
		echo "export PATH"
		echo "export VAGRANT_DEFAULT_PROVIDER=parallels"
		echo "alias ls='ls -Gh'"
		echo "alias zshconfig='nano ${file}'"
		echo "alias ohmyzsh='nano ${file}/.oh-my-zsh'"
		echo "alias showFiles='defaults write com.apple.finder AppleShowAllFiles YES; killall Finder /System/Library/CoreServices/Finder.app'"
		echo "alias hideFiles='defaults write com.apple.finder AppleShowAllFiles NO; killall Finder /System/Library/CoreServices/Finder.app'"
		echo "autoload -U promptinit"
		echo "promptinit"
		echo "export WORKON_HOME=~/${WORKON_HOME}"
		echo "export PROJECT_HOME=~/${PROJECT_HOME}"
		echo "export VIRTUALENVWRAPPER_PYTHON=$(which python3)"
		echo "source /Library/Frameworks/Python.framework/Versions/3.4/bin/virtualenvwrapper.sh"
		echo "source $WORKON_HOME/preactivate"
		echo "source /Library/Frameworks/Python.framework/Versions/3.4/bin/activate.sh"	# autoenv
		echo ""
	} >> ${file}
fi
source ${file}
